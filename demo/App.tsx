import React, { FC } from "react";
import { Box } from "./components/Box";
import { GlobalStyles } from "./components/GlobalStyles";

export const App: FC = () => {
    return (
        <>
            <GlobalStyles />
            <Box $backgroundColor="#f00" $height={[40, null, null, 100]} $hideVisually={{ from: "md", to: "lg" }}>
                Test
            </Box>
        </>
    );
};
