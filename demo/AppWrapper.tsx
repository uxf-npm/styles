import React, { FC } from "react";
import { ThemeProvider } from "styled-components";
import { theme } from "./theme";

export const AppWrapper: FC = (props) => {
    const { children } = props;

    return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
