import { createGlobalStyle } from "styled-components";
import { reboot } from "../../../src/mixins/reboot";

export const GlobalStyles = createGlobalStyle`
    ${reboot()}
`;
