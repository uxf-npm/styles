import { HTMLAttributes } from "react";
import styled, { css, FlattenSimpleInterpolation } from "styled-components";
import { formatResponsiveValue } from "../../../src/utils/formatValue";
import { facepaintMin } from "../../../src/utils/responsive";
import { injectCss } from "../../../src/mixins/injectCss";
import { Child, child } from "../../../src/properties/child";
import { colors, Colors } from "../../../src/properties/colors";
import { Column, column } from "../../../src/properties/column";
import { Display, display } from "../../../src/properties/display";
import { Position, position } from "../../../src/properties/position";
import { Sizing, sizing } from "../../../src/properties/sizing";
import { spacings, Spacings } from "../../../src/properties/spacings";
import { visibility, Visibility } from "../../../src/properties/visibility";
import { ResponsiveProperty } from "../../../src/types";
import { Theme } from "../../theme";

type SelectedBoxElements =
    | "article"
    | "aside"
    | "div"
    | "dl"
    | "fieldset"
    | "figure"
    | "footer"
    | "form"
    | "header"
    | "h1"
    | "h2"
    | "h3"
    | "h4"
    | "h5"
    | "h6"
    | "main"
    | "nav"
    | "ol"
    | "p"
    | "section"
    | "span"
    | "ul";

export interface BoxProps
    extends HTMLAttributes<HTMLDivElement>,
        Child,
        Colors,
        Column,
        Display,
        Position,
        Sizing,
        Spacings,
        Visibility<Theme["breakpoint"]> {
    as?: keyof Pick<JSX.IntrinsicElements, SelectedBoxElements>;
    $borderRadius?: ResponsiveProperty.BorderRadius;
    $boxShadow?: ResponsiveProperty.BoxShadow;
    $breakInside?: ResponsiveProperty.BreakInside;
    $css?: FlattenSimpleInterpolation;
    $overflow?: ResponsiveProperty.Overflow;
}

export const Box = styled("div")<BoxProps>(
    ({
        $borderRadius,
        $boxShadow,
        $breakInside,
        $css,
        $display = "block",
        $minWidth = 0,
        $overflow,
        theme,
        ...restProps
    }) => css`
        ${facepaintMin(theme.breakpoint)({
            borderRadius: formatResponsiveValue($borderRadius),
            boxShadow: formatResponsiveValue($boxShadow),
            breakInside: formatResponsiveValue($breakInside),
            overflow: formatResponsiveValue($overflow),
        })}
        ${child({
            breakpoints: theme.breakpoint,
            ...restProps,
        })}
        ${colors({ breakpoints: theme.breakpoint, ...restProps })}
        ${column({ breakpoints: theme.breakpoint, ...restProps })}
        ${display({ breakpoints: theme.breakpoint, $display })}
        ${position({ breakpoints: theme.breakpoint, ...restProps })}
        ${child({
            breakpoints: theme.breakpoint,
            ...restProps,
        })}
        ${sizing({
            breakpoints: theme.breakpoint,
            $minWidth,
            ...restProps,
        })}
        ${spacings({ breakpoints: theme.breakpoint, ...restProps })}
        ${visibility({ breakpoints: theme.breakpoint, ...restProps })}
        ${injectCss($css)}
    `,
);
