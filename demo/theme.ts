export const theme = {
    breakpoint: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200,
        xxl: 1440,
    },
    container: {
        xs: 420,
        sm: 540,
        md: 720,
        lg: 960,
        xl: 1060,
    },
    zIndex: {
        focus: 5,
        fixed: 10,
        modal: 100,
        dropdown: 200,
        flashMessage: 250,
        tooltip: 300,
    },
};

export type Theme = typeof theme;
