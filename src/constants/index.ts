import { CLASSES } from "@uxf/core/constants/classes";

/** @deprecated Moved to @uxf/core package and renamed.
 * Use 'import { CLASSES } from "@uxf/core/constants/classes";' instead. */
export const Classes = CLASSES;
