import { Globals, Property } from "csstype";
import { CSSProperties } from "react";

export type CssTimeUnits = "ms" | "s";
export type CssAbsoluteLengthsUnits = "cm" | "mm" | "in" | "px" | "pt" | "pc" | "deg" | "rad";
export type CssRelativeLengthsUnits = "em" | "ex" | "ch" | "rem" | "vw" | "vh" | "vmin" | "vmax" | "%";
export type CssUnits = CssTimeUnits | CssAbsoluteLengthsUnits | CssRelativeLengthsUnits;

export type BreakpointsType = Record<string, number>;

export interface Breakpoints<B extends BreakpointsType> {
    breakpoints: B;
}

export interface ResponsiveRange<B extends BreakpointsType> {
    from?: keyof B;
    to?: keyof B;
}

export type ResponsiveConditionProperty<B extends BreakpointsType> = boolean | ResponsiveRange<B>;

interface MaxLinesResponsiveRange<B extends BreakpointsType> extends ResponsiveRange<B> {
    count: number;
}

export type ResponsiveMaxLinesProperty<B extends BreakpointsType> = number | MaxLinesResponsiveRange<B>;

export type ColorProperty = Globals | "currentcolor" | "transparent" | string;

type BgPosition = "bottom" | "center" | "left" | "right" | "top";
type BgRepeatStyle = "no-repeat" | "repeat" | "repeat-x" | "repeat-y" | "round" | "space";
type BgAttachment = "fixed" | "local" | "scroll";
type BgBox = "border-box" | "content-box" | "padding-box";

export type BackgroundProperty = ColorProperty | BgPosition | BgRepeatStyle | BgAttachment | BgBox | "none" | string;
export type TransitionProperty = keyof CSSProperties;
export type TransitionProperties = TransitionProperty | TransitionProperty[];

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace ResponsiveProperty {
    export type AlignContent = Property.AlignContent | (Property.AlignContent | null)[];
    export type AlignItems = Property.AlignItems | (Property.AlignItems | null)[];
    export type AlignSelf = Property.AlignSelf | (Property.AlignSelf | null)[];
    export type AspectRatio = number | (number | null)[];
    export type BackfaceVisibility = Property.BackfaceVisibility | (Property.BackfaceVisibility | null)[];
    export type Background = BackgroundProperty | (BackgroundProperty | null)[];
    export type Border = Property.Border<number> | (Property.Border<number> | null)[];
    export type BorderRadius = Property.BorderRadius<number> | (Property.BorderRadius<number> | null)[];
    export type Bottom = Property.Bottom<number> | (Property.Bottom<number> | null)[];
    export type BoxShadow = Property.BoxShadow | (Property.BoxShadow | null)[];
    export type BreakAfter = Property.BreakAfter | (Property.BreakAfter | null)[];
    export type BreakBefore = Property.BreakBefore | (Property.BreakBefore | null)[];
    export type BreakInside = Property.BreakInside | (Property.BreakInside | null)[];
    export type Clear = Property.Clear | (Property.Clear | null)[];
    export type Color = ColorProperty | (ColorProperty | null)[];
    export type ColumnCount = Property.ColumnCount | (Property.ColumnCount | null)[];
    export type ColumnFill = Property.ColumnFill | (Property.ColumnFill | null)[];
    export type ColumnGap = Property.ColumnGap<number> | (Property.ColumnGap<number> | null)[];
    export type ColumnRule = Property.ColumnRule<number> | (Property.ColumnRule<number> | null)[];
    export type ColumnSpan = Property.ColumnSpan | (Property.ColumnSpan | null)[];
    export type ColumnWidth = Property.ColumnWidth<number | string> | (Property.ColumnWidth<number | string> | null)[];
    export type Cursor = Property.Cursor | (Property.Cursor | null)[];
    export type Display = Property.Display | (Property.Display | null)[];
    export type FlexBasis = Property.FlexBasis<number> | (Property.FlexBasis<number> | null)[];
    export type FlexDirection = Property.FlexDirection | (Property.FlexDirection | null)[];
    export type FlexGrow = Property.FlexGrow | (Property.FlexGrow | null)[];
    export type FlexShrink = Property.FlexShrink | (Property.FlexShrink | null)[];
    export type FlexWrap = Property.FlexWrap | (Property.FlexWrap | null)[];
    export type Float = Property.Float | (Property.Float | null)[];
    export type FontFamily = Property.FontFamily | (Property.FontFamily | null)[];
    export type FontSize = Property.FontSize<number> | (Property.FontSize<number> | null)[];
    export type FontStretch = Property.FontStretch | (Property.FontStretch | null)[];
    export type FontStyle = Property.FontStyle | (Property.FontStyle | null)[];
    export type FontWeight = Property.FontWeight | (Property.FontWeight | null)[];
    export type GridArea = Property.GridArea | (Property.GridArea | null)[];
    export type GridAutoColumns = Property.GridAutoColumns<number> | (Property.GridAutoColumns<number> | null)[];
    export type GridAutoFlow = Property.GridAutoFlow | (Property.GridAutoFlow | null)[];
    export type GridAutoRows = Property.GridAutoRows<number> | (Property.GridAutoRows<number> | null)[];
    export type GridColumnCount = number | (number | null)[];
    export type GridColumnGap = Property.GridColumnGap<number> | (Property.GridColumnGap<number> | null)[]; // change to `ColumnGap` in future
    export type GridGap = Property.GridGap<number> | (Property.GridGap<number> | null)[]; // change to `Gap` in future
    export type GridRowCount = number | (number | null)[];
    export type GridRowGap = Property.GridRowGap<number> | (Property.GridRowGap<number> | null)[]; // change to `RowGap` in future
    export type GridTemplateAreas = Property.GridTemplateAreas | (Property.GridTemplateAreas | null)[];
    export type GridTemplateColumns =
        | Property.GridTemplateColumns<number>
        | (Property.GridTemplateColumns<number> | null)[];
    export type GridTemplateRows = Property.GridTemplateRows<number> | (Property.GridTemplateRows<number> | null)[];
    export type Height = Property.Height<number> | (Property.Height<number> | null)[];
    export type Hyphens = Property.Hyphens | (Property.Hyphens | null)[];
    export type JustifyContent = Property.JustifyContent | (Property.JustifyContent | null)[];
    export type JustifyItems = Property.JustifyItems | (Property.JustifyItems | null)[];
    export type JustifySelf = Property.JustifySelf | (Property.JustifySelf | null)[];
    export type Left = Property.Left<number> | (Property.Left<number> | null)[];
    export type LetterSpacing =
        | Property.LetterSpacing<number | string>
        | (Property.LetterSpacing<number | string> | null)[];
    export type LineHeight = Property.LineHeight<number> | (Property.LineHeight<number> | null)[];
    export type Margin = Property.Margin<number> | (Property.Margin<number> | null)[];
    export type MaxHeight = Property.MaxHeight<number> | (Property.MaxHeight<number> | null)[];
    export type MaxWidth = Property.MaxWidth<number> | (Property.MaxWidth<number> | null)[];
    export type MinHeight = Property.MinHeight<number> | (Property.MinHeight<number> | null)[];
    export type MinWidth = Property.MinWidth<number> | (Property.MinWidth<number> | null)[];
    export type ObjectFit = Property.ObjectFit | (Property.ObjectFit | null)[];
    export type ObjectPosition = Property.ObjectPosition | (Property.ObjectPosition | null)[];
    export type Opacity = Property.Opacity | (Property.Opacity | null)[];
    export type Order = Property.Order | (Property.Order | null)[];
    export type Outline = Property.Outline<number> | (Property.Outline<number> | null)[];
    export type Overflow = Property.Overflow | (Property.Overflow | null)[];
    export type Padding = Property.Padding<number> | (Property.Padding<number> | null)[];
    export type Perspective = Property.Perspective | (Property.Perspective | null)[];
    export type PerspectiveOrigin = Property.PerspectiveOrigin | (Property.PerspectiveOrigin | null)[];
    export type PointerEvents = Property.PointerEvents | (Property.PointerEvents | null)[];
    export type Position = Property.Position | (Property.Position | null)[];
    export type Resize = Property.Resize | (Property.Resize | null)[];
    export type Right = Property.Right<number> | (Property.Right<number> | null)[];
    export type TextAlign = Property.TextAlign | (Property.TextAlign | null)[];
    export type TextDecoration = Property.TextDecoration<string> | (Property.TextDecoration<string> | null)[];
    export type TextIndent = Property.TextIndent<number> | (Property.TextIndent<number> | null)[];
    export type TextShadow = Property.TextShadow | (Property.TextShadow | null)[];
    export type TextTransform = Property.TextTransform | (Property.TextTransform | null)[];
    export type Top = Property.Top<number> | (Property.Top<number> | null)[];
    export type Transform = Property.Transform | (Property.Transform | null)[];
    export type TransformOrigin = Property.TransformOrigin | (Property.TransformOrigin | null)[];
    export type TransformStyle = Property.TransformStyle | (Property.TransformStyle | null)[];
    export type UserSelect = Property.UserSelect | (Property.UserSelect | null)[];
    export type VerticalAlign = Property.VerticalAlign | (Property.VerticalAlign | null)[];
    export type Width = Property.Width<number> | (Property.Width<number> | null)[];
    export type ZIndex = Property.ZIndex | (Property.ZIndex | null)[];
}
