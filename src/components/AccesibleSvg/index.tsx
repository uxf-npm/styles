import { slugify } from "@uxf/core/utils/slugify";
import React, { FC, memo, ReactNode, SVGAttributes } from "react";

export interface AccessibleSvgProps extends SVGAttributes<SVGElement> {
    children: ReactNode;
    description?: string;
    id: string;
    title: string;
    viewBox: string;
}

const AccessibleSvgComponent: FC<AccessibleSvgProps> = (props) => {
    const {
        children,
        description,
        id,
        preserveAspectRatio = "xMidYMid meet",
        role = "img",
        title,
        viewBox,
        ...restProps
    } = props;

    const idPrefix = `svg__${slugify(id)}`;
    const titleId = `${idPrefix}__title`;
    const descriptionId = `${idPrefix}__desc`;
    const ariaLabelledBy = [titleId, description].filter((i) => i).join(" ");

    return (
        <svg
            aria-labelledby={ariaLabelledBy}
            id={id}
            preserveAspectRatio={preserveAspectRatio}
            role={role}
            viewBox={viewBox}
            {...restProps}
        >
            <title id={titleId}>{title}</title>
            {!!description && <desc id={descriptionId}>{description}</desc>}
            {children}
        </svg>
    );
};

export const AccessibleSvg = memo(AccessibleSvgComponent);

AccessibleSvg.displayName = "AccessibleSvg";
