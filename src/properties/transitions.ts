import { Property } from "csstype";
import { css } from "styled-components";
import { TransitionProperties } from "../types";
import { transition } from "../utils/transition";

export interface Transitions {
    $transition?: { property: TransitionProperties; duration?: number; easing?: Property.TransitionTimingFunction };
}

export const transitions = ({ $transition }: Transitions) => css`
    transition: ${$transition ? transition($transition.property, $transition.duration, $transition.easing) : null};
`;
