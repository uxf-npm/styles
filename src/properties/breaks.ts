import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Breaks {
    $breakAfter?: ResponsiveProperty.BreakAfter;
    $breakBefore?: ResponsiveProperty.BreakBefore;
    $breakInside?: ResponsiveProperty.BreakInside;
}

export const breaks = <B extends BreakpointsType>({
    breakpoints,
    $breakAfter,
    $breakBefore,
    $breakInside,
}: Breaks & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        breakAfter: formatResponsiveValue($breakAfter),
        breakBefore: formatResponsiveValue($breakBefore),
        breakInside: formatResponsiveValue($breakInside),
    });
