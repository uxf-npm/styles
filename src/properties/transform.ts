import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Transform {
    $transform?: ResponsiveProperty.Transform;
    $transformOrigin?: ResponsiveProperty.TransformOrigin;
    $transformStyle?: ResponsiveProperty.TransformStyle;
}

export const transform = <B extends BreakpointsType>({
    breakpoints,
    $transform,
    $transformOrigin,
    $transformStyle,
}: Transform & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        transform: formatResponsiveValue($transform),
        transformOrigin: formatResponsiveValue($transformOrigin),
        transformStyle: formatResponsiveValue($transformStyle),
    });
