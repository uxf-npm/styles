import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Perspective {
    $perspective?: ResponsiveProperty.Perspective;
    $perspectiveOrigin?: ResponsiveProperty.PerspectiveOrigin;
}

export const perspective = <B extends BreakpointsType>({
    breakpoints,
    $perspective,
    $perspectiveOrigin,
}: Perspective & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        perspective: formatResponsiveValue($perspective),
        perspectiveOrigin: formatResponsiveValue($perspectiveOrigin),
    });
