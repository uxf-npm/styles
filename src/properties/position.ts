import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Position {
    $bottom?: ResponsiveProperty.Bottom;
    $left?: ResponsiveProperty.Left;
    $right?: ResponsiveProperty.Right;
    $top?: ResponsiveProperty.Top;
    $position?: ResponsiveProperty.Position;
    $verticalAlign?: ResponsiveProperty.VerticalAlign;
    $zIndex?: ResponsiveProperty.ZIndex;
}

export const position = <B extends BreakpointsType>({
    breakpoints,
    $bottom,
    $left,
    $right,
    $top,
    $position,
    $verticalAlign,
    $zIndex,
}: Position & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        bottom: formatResponsiveValue($bottom),
        left: formatResponsiveValue($left),
        right: formatResponsiveValue($right),
        top: formatResponsiveValue($top),
        position: formatResponsiveValue($position),
        verticalAlign: formatResponsiveValue($verticalAlign),
        zIndex: formatResponsiveValue($zIndex, true),
    });
