import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface TextDecoration {
    $textDecoration?: ResponsiveProperty.TextDecoration;
}

export const textDecoration = <B extends BreakpointsType>({
    breakpoints,
    $textDecoration,
}: TextDecoration & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        textDecoration: formatResponsiveValue($textDecoration),
    });
