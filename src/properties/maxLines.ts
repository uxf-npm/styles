import { css } from "styled-components";
import { responsiveMultilineTruncate } from "../mixins/responsiveMultilineTruncate";
import { Breakpoints, BreakpointsType, ResponsiveMaxLinesProperty } from "../types";

export interface MaxLines<B extends BreakpointsType> {
    $maxLines?: ResponsiveMaxLinesProperty<B>;
}

export const maxLines = <B extends BreakpointsType>({ breakpoints, $maxLines }: MaxLines<B> & Breakpoints<B>) => css`
    ${$maxLines
        ? responsiveMultilineTruncate(
              breakpoints,
              typeof $maxLines === "number" ? $maxLines : $maxLines.count,
              typeof $maxLines === "number" ? !!$maxLines : { from: $maxLines.from, to: $maxLines.to },
          )
        : null}
`;
