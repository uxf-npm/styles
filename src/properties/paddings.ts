import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Paddings {
    $pb?: ResponsiveProperty.Padding;
    $pl?: ResponsiveProperty.Padding;
    $pr?: ResponsiveProperty.Padding;
    $pt?: ResponsiveProperty.Padding;
    $py?: ResponsiveProperty.Padding;
    $px?: ResponsiveProperty.Padding;
}

export const paddings = <B extends BreakpointsType>({
    breakpoints,
    $pb,
    $pl,
    $pr,
    $pt,
    $py,
    $px,
}: Paddings & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        paddingBottom: formatResponsiveValue($pb ?? $py),
        paddingLeft: formatResponsiveValue($pl ?? $px),
        paddingRight: formatResponsiveValue($pr ?? $px),
        paddingTop: formatResponsiveValue($pt ?? $py),
    });
