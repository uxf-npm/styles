import { css } from "styled-components";
import { BreakpointsType, Breakpoints } from "../types";
import { margins, Margins } from "./margins";
import { paddings, Paddings } from "./paddings";

export interface Spacings extends Margins, Paddings {}

export const spacings = <B extends BreakpointsType>(props: Margins & Paddings & Breakpoints<B>) => css`
    ${margins(props)}
    ${paddings(props)}
`;
