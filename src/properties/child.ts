import { Breakpoints, BreakpointsType } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";
import { FlexChild } from "./flexChild";
import { GridChild } from "./gridChild";

export interface Child extends FlexChild, GridChild {}

export const child = <B extends BreakpointsType>({
    breakpoints,
    $alignSelf,
    $flexBasis,
    $flexGrow,
    $flexShrink,
    $gridArea,
    $justifySelf,
    $order,
}: Child & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        alignSelf: formatResponsiveValue($alignSelf),
        flexBasis: formatResponsiveValue($flexBasis),
        flexGrow: formatResponsiveValue($flexGrow, true),
        flexShrink: formatResponsiveValue($flexShrink, true),
        gridArea: formatResponsiveValue($gridArea),
        justifySelf: formatResponsiveValue($justifySelf),
        order: formatResponsiveValue($order, true),
    });
