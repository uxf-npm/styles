import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Column {
    $columnCount?: ResponsiveProperty.ColumnCount;
    $columnFill?: ResponsiveProperty.ColumnFill;
    $columnGap?: ResponsiveProperty.ColumnGap;
    $columnRule?: ResponsiveProperty.ColumnRule;
    $columnSpan?: ResponsiveProperty.ColumnSpan;
    $columnWidth?: ResponsiveProperty.ColumnWidth;
}

export const column = <B extends BreakpointsType>({
    breakpoints,
    $columnCount,
    $columnFill,
    $columnGap,
    $columnRule,
    $columnSpan,
    $columnWidth,
}: Column & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        columnCount: formatResponsiveValue($columnCount, true),
        columnFill: formatResponsiveValue($columnFill),
        columnGap: formatResponsiveValue($columnGap),
        columnRule: formatResponsiveValue($columnRule),
        columnSpan: formatResponsiveValue($columnSpan),
        columnWidth: formatResponsiveValue($columnWidth),
    });
