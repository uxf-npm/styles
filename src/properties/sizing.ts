import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Sizing {
    $height?: ResponsiveProperty.Height;
    $maxHeight?: ResponsiveProperty.MaxHeight;
    $maxWidth?: ResponsiveProperty.MaxWidth;
    $minHeight?: ResponsiveProperty.MinHeight;
    $minWidth?: ResponsiveProperty.MinWidth;
    $width?: ResponsiveProperty.Width;
}

export const sizing = <B extends BreakpointsType>({
    breakpoints,
    $height,
    $maxHeight,
    $maxWidth,
    $minHeight,
    $minWidth,
    $width,
}: Sizing & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        height: formatResponsiveValue($height),
        maxHeight: formatResponsiveValue($maxHeight),
        maxWidth: formatResponsiveValue($maxWidth),
        minHeight: formatResponsiveValue($minHeight),
        minWidth: formatResponsiveValue($minWidth),
        width: formatResponsiveValue($width),
    });
