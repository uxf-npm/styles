import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Margins {
    $mb?: ResponsiveProperty.Margin;
    $ml?: ResponsiveProperty.Margin;
    $mr?: ResponsiveProperty.Margin;
    $mt?: ResponsiveProperty.Margin;
    $my?: ResponsiveProperty.Margin;
    $mx?: ResponsiveProperty.Margin;
}

export const margins = <B extends BreakpointsType>({
    breakpoints,
    $mb,
    $ml,
    $mr,
    $mt,
    $my,
    $mx,
}: Margins & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        marginBottom: formatResponsiveValue($mb ?? $my),
        marginLeft: formatResponsiveValue($ml ?? $mx),
        marginRight: formatResponsiveValue($mr ?? $mx),
        marginTop: formatResponsiveValue($mt ?? $my),
    });
