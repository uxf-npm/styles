import { css } from "styled-components";
import { Breakpoints, BreakpointsType } from "../types";
import { character, Character } from "./character";
import { colors, Colors } from "./colors";
import { paragraph, Paragraph } from "./paragraph";
import { textDecoration } from "./textDecoration";

export interface Typography<B extends BreakpointsType> extends Character, Paragraph<B>, Colors {}

export const typography = <B extends BreakpointsType>(props: Typography<B> & Breakpoints<B>) => css`
    ${character(props)}
    ${colors(props)}
    ${paragraph(props)}
    ${textDecoration(props)}
`;
