import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface FlexParent {
    $alignContent?: ResponsiveProperty.AlignContent;
    $alignItems?: ResponsiveProperty.AlignItems;
    $display?: ResponsiveProperty.Display;
    $flexDirection?: ResponsiveProperty.FlexDirection;
    $flexWrap?: ResponsiveProperty.FlexWrap;
    $justifyContent?: ResponsiveProperty.JustifyContent;
}

export const flexParent = <B extends BreakpointsType>({
    breakpoints,
    $alignContent,
    $alignItems,
    $display,
    $flexDirection,
    $flexWrap,
    $justifyContent,
}: FlexParent & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        alignContent: formatResponsiveValue($alignContent),
        alignItems: formatResponsiveValue($alignItems),
        display: formatResponsiveValue($display ?? "flex"),
        flexDirection: formatResponsiveValue($flexDirection),
        flexWrap: formatResponsiveValue($flexWrap),
        justifyContent: formatResponsiveValue($justifyContent),
    });
