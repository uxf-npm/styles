import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Display {
    $display?: ResponsiveProperty.Display;
}

export const display = <B extends BreakpointsType>({ breakpoints, $display }: Display & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        display: formatResponsiveValue($display),
    });
