import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Shadows {
    $boxShadow?: ResponsiveProperty.BoxShadow;
    $textShadow?: ResponsiveProperty.TextShadow;
}

export const shadows = <B extends BreakpointsType>({
    breakpoints,
    $boxShadow,
    $textShadow,
}: Shadows & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        boxShadow: formatResponsiveValue($boxShadow),
        textShadow: formatResponsiveValue($textShadow),
    });
