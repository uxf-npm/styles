import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Overflow {
    $overflow?: ResponsiveProperty.Overflow;
    $overflowX?: ResponsiveProperty.Overflow;
    $overflowY?: ResponsiveProperty.Overflow;
}

export const overflow = <B extends BreakpointsType>({
    breakpoints,
    $overflow,
    $overflowX,
    $overflowY,
}: Overflow & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        overflow: formatResponsiveValue($overflow),
        overflowX: formatResponsiveValue($overflowX),
        overflowY: formatResponsiveValue($overflowY),
    });
