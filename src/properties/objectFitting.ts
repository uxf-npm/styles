import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface ObjectFitting {
    $objectFit?: ResponsiveProperty.ObjectFit;
    $objectPosition?: ResponsiveProperty.ObjectPosition;
}

export const objectFitting = <B extends BreakpointsType>({
    breakpoints,
    $objectFit,
    $objectPosition,
}: ObjectFitting & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        objectFit: formatResponsiveValue($objectFit),
        objectPosition: formatResponsiveValue($objectPosition),
    });
