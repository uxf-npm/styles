import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Character {
    $fontFamily?: ResponsiveProperty.FontFamily;
    $fontSize?: ResponsiveProperty.FontSize;
    $fontStretch?: ResponsiveProperty.FontStretch;
    $fontStyle?: ResponsiveProperty.FontStyle;
    $fontWeight?: ResponsiveProperty.FontWeight;
    $letterSpacing?: ResponsiveProperty.LetterSpacing;
    $lineHeight?: ResponsiveProperty.LineHeight;
    $textTransform?: ResponsiveProperty.TextTransform;
}

export const character = <B extends BreakpointsType>({
    breakpoints,
    $fontFamily,
    $fontSize,
    $fontStretch,
    $fontStyle,
    $fontWeight,
    $letterSpacing,
    $lineHeight,
    $textTransform,
}: Character & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        fontFamily: formatResponsiveValue($fontFamily),
        fontSize: formatResponsiveValue($fontSize),
        fontStretch: formatResponsiveValue($fontStretch),
        fontStyle: formatResponsiveValue($fontStyle),
        fontWeight: formatResponsiveValue($fontWeight, true),
        letterSpacing: formatResponsiveValue($letterSpacing),
        lineHeight: formatResponsiveValue($lineHeight),
        textTransform: formatResponsiveValue($textTransform),
    });
