import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Background {
    $backgroundColor?: ResponsiveProperty.Color;
    $background?: ResponsiveProperty.Background;
}

export const background = <B extends BreakpointsType>({
    breakpoints,
    $backgroundColor,
    $background,
}: Background & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        backgroundColor: formatResponsiveValue($backgroundColor),
        background: formatResponsiveValue($background),
    });
