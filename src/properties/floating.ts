import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Floating {
    $clear?: ResponsiveProperty.Clear;
    $float?: ResponsiveProperty.Float;
}

export const floating = <B extends BreakpointsType>({ breakpoints, $clear, $float }: Floating & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        clear: formatResponsiveValue($clear),
        float: formatResponsiveValue($float),
    });
