import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface GridChild {
    $alignSelf?: ResponsiveProperty.AlignSelf;
    $gridArea?: ResponsiveProperty.GridArea;
    $justifySelf?: ResponsiveProperty.JustifySelf;
}

export const gridChild = <B extends BreakpointsType>({
    breakpoints,
    $alignSelf,
    $gridArea,
    $justifySelf,
}: GridChild & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        alignSelf: formatResponsiveValue($alignSelf),
        gridArea: formatResponsiveValue($gridArea),
        justifySelf: formatResponsiveValue($justifySelf),
    });
