import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Borders {
    $border?: ResponsiveProperty.Border;
    $borderBottom?: ResponsiveProperty.Border;
    $borderLeft?: ResponsiveProperty.Border;
    $borderRight?: ResponsiveProperty.Border;
    $borderTop?: ResponsiveProperty.Border;
    $borderX?: ResponsiveProperty.Border;
    $borderY?: ResponsiveProperty.Border;
    $borderRadius?: ResponsiveProperty.BorderRadius;
    $outline?: ResponsiveProperty.Outline;
}

export const borders = <B extends BreakpointsType>({
    breakpoints,
    $border,
    $borderBottom,
    $borderLeft,
    $borderRight,
    $borderTop,
    $borderX,
    $borderY,
    $borderRadius,
    $outline,
}: Borders & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        border: formatResponsiveValue($border),
        borderBottom: formatResponsiveValue($borderBottom ?? $borderY),
        borderLeft: formatResponsiveValue($borderLeft ?? $borderX),
        borderRight: formatResponsiveValue($borderRight ?? $borderX),
        borderTop: formatResponsiveValue($borderTop ?? $borderY),
        borderRadius: formatResponsiveValue($borderRadius),
        outline: formatResponsiveValue($outline),
    });
