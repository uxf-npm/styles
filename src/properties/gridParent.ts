import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveGridColumns } from "../utils/formatGridColumns";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface GridParent {
    $alignContent?: ResponsiveProperty.AlignContent;
    $alignItems?: ResponsiveProperty.AlignItems;
    $display?: ResponsiveProperty.Display;
    $gridAutoColumns?: ResponsiveProperty.GridAutoColumns;
    $gridAutoFlow?: ResponsiveProperty.GridAutoFlow;
    $gridAutoRows?: ResponsiveProperty.GridAutoRows;
    $gridColumnCount?: ResponsiveProperty.GridColumnCount;
    $gridGap?: ResponsiveProperty.GridGap; // change to `$gap` in future
    $gridColumnGap?: ResponsiveProperty.GridColumnGap; // change to `$columnGap` in future
    $gridRowGap?: ResponsiveProperty.GridRowGap; // change to `$rowGap` in future
    $gridRowCount?: ResponsiveProperty.GridRowCount;
    $gridTemplateAreas?: ResponsiveProperty.GridTemplateAreas;
    $gridTemplateColumns?: ResponsiveProperty.GridTemplateColumns;
    $gridTemplateRows?: ResponsiveProperty.GridTemplateRows;
    $justifyContent?: ResponsiveProperty.JustifyContent;
    $justifyItems?: ResponsiveProperty.JustifyItems;
}

export const gridParent = <B extends BreakpointsType>({
    breakpoints,
    $alignContent,
    $alignItems,
    $display,
    $gridAutoColumns,
    $gridAutoFlow,
    $gridAutoRows,
    $gridColumnCount,
    $gridGap,
    $gridColumnGap,
    $gridRowGap,
    $gridRowCount,
    $gridTemplateAreas,
    $gridTemplateColumns,
    $gridTemplateRows,
    $justifyContent,
    $justifyItems,
}: GridParent & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        alignContent: formatResponsiveValue($alignContent),
        alignItems: formatResponsiveValue($alignItems),
        display: formatResponsiveValue($display ?? "grid"),
        gridAutoColumns: formatResponsiveValue($gridAutoColumns),
        gridAutoFlow: formatResponsiveValue($gridAutoFlow),
        gridAutoRows: formatResponsiveValue($gridAutoRows),
        gridGap: formatResponsiveValue($gridGap), // must be before `gridColumnGap` and `gridRowGap` for proper overriding
        gridColumnGap: formatResponsiveValue($gridColumnGap),
        gridRowGap: formatResponsiveValue($gridRowGap),
        gridTemplateAreas: formatResponsiveValue($gridTemplateAreas),
        gridTemplateColumns: $gridTemplateColumns
            ? formatResponsiveValue($gridTemplateColumns)
            : formatResponsiveGridColumns($gridColumnCount),
        gridTemplateRows: $gridTemplateRows
            ? formatResponsiveValue($gridTemplateRows)
            : formatResponsiveGridColumns($gridRowCount),
        justifyContent: formatResponsiveValue($justifyContent),
        justifyItems: formatResponsiveValue($justifyItems),
    });
