import { css } from "styled-components";
import { responsiveHidden } from "../mixins/responsiveHidden";
import { responsiveHideVisually } from "../mixins/responsiveHideVisually";
import { Breakpoints, BreakpointsType, ResponsiveConditionProperty, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Visibility<B extends BreakpointsType> {
    $backfaceVisibility?: ResponsiveProperty.BackfaceVisibility;
    $hidden?: ResponsiveConditionProperty<B>;
    $opacity?: ResponsiveProperty.Opacity;
    $hideVisually?: ResponsiveConditionProperty<B>;
}

export const visibility = <B extends BreakpointsType>({
    breakpoints,
    $backfaceVisibility,
    $hidden,
    $opacity,
    $hideVisually,
}: Visibility<B> & Breakpoints<B>) => css`
    ${resolveResponsiveProperties(breakpoints, {
        backfaceVisibility: formatResponsiveValue($backfaceVisibility),
        opacity: formatResponsiveValue($opacity),
    })}
    ${responsiveHideVisually(breakpoints, $hideVisually)}
    ${responsiveHidden(breakpoints, $hidden)}
`;
