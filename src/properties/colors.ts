import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Colors {
    $backgroundColor?: ResponsiveProperty.Color;
    $color?: ResponsiveProperty.Color;
}

export const colors = <B extends BreakpointsType>({ breakpoints, $backgroundColor, $color }: Colors & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        backgroundColor: formatResponsiveValue($backgroundColor),
        color: formatResponsiveValue($color),
    });
