import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface Interactions {
    $cursor?: ResponsiveProperty.Cursor;
    $pointerEvents?: ResponsiveProperty.PointerEvents;
    $resize?: ResponsiveProperty.Resize;
    $userSelect?: ResponsiveProperty.UserSelect;
}

export const interactions = <B extends BreakpointsType>({
    breakpoints,
    $cursor,
    $pointerEvents,
    $resize,
    $userSelect,
}: Interactions & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        cursor: formatResponsiveValue($cursor),
        pointerEvents: formatResponsiveValue($pointerEvents),
        resize: formatResponsiveValue($resize),
        userSelect: formatResponsiveValue($userSelect),
    });
