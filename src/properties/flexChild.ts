import { Breakpoints, BreakpointsType, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";

export interface FlexChild {
    $alignSelf?: ResponsiveProperty.AlignSelf;
    $flexBasis?: ResponsiveProperty.FlexBasis;
    $flexGrow?: ResponsiveProperty.FlexGrow;
    $flexShrink?: ResponsiveProperty.FlexShrink;
    $order?: ResponsiveProperty.Order;
}

export const flexChild = <B extends BreakpointsType>({
    breakpoints,
    $alignSelf,
    $flexBasis,
    $flexGrow,
    $flexShrink,
    $order,
}: FlexChild & Breakpoints<B>) =>
    resolveResponsiveProperties(breakpoints, {
        alignSelf: formatResponsiveValue($alignSelf),
        flexBasis: formatResponsiveValue($flexBasis),
        flexGrow: formatResponsiveValue($flexGrow, true),
        flexShrink: formatResponsiveValue($flexShrink, true),
        order: formatResponsiveValue($order, true),
    });
