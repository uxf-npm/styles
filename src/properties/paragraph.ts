import { css } from "styled-components";
import { responsiveNoWrap } from "../mixins/responsiveNoWrap";
import { responsiveTruncate } from "../mixins/responsiveTruncate";
import { Breakpoints, BreakpointsType, ResponsiveConditionProperty, ResponsiveProperty } from "../types";
import { formatResponsiveValue } from "../utils/formatValue";
import { resolveResponsiveProperties } from "../utils/responsive";
import { MaxLines, maxLines } from "./maxLines";

export interface Paragraph<B extends BreakpointsType> extends MaxLines<B> {
    $hyphens?: ResponsiveProperty.Hyphens;
    $textAlign?: ResponsiveProperty.TextAlign;
    $textIndent?: ResponsiveProperty.TextIndent;
    $truncate?: ResponsiveConditionProperty<B>;
    $noWrap?: ResponsiveConditionProperty<B>;
}

export const paragraph = <B extends BreakpointsType>({
    breakpoints,
    $hyphens,
    $maxLines,
    $noWrap,
    $textAlign,
    $textIndent,
    $truncate,
}: Paragraph<B> & Breakpoints<B>) => css`
    ${resolveResponsiveProperties(breakpoints, {
        hyphens: formatResponsiveValue($hyphens),
        textAlign: formatResponsiveValue($textAlign),
        textIndent: formatResponsiveValue($textIndent),
    })}
    ${maxLines({ breakpoints, $maxLines })}
    ${responsiveTruncate(breakpoints, $truncate)}
    ${responsiveNoWrap(breakpoints, $noWrap)}
`;
