export const encodedSvg = (svg: string) => `url("data:image/svg+xml; utf8, ${escape(svg)}")`;
