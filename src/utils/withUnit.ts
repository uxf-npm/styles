import { CssUnits } from "../types";

export const withUnit = (input: number | string, unit: CssUnits) => `${input}${unit}`;
