import { camelCaseToDash } from "@uxf/core/utils/camelCaseToDash";
import { Property } from "csstype";
import { TransitionProperties } from "../types";
import { withUnit } from "./withUnit";

export const transition = (
    property: TransitionProperties,
    duration = 400,
    easing: Property.TransitionTimingFunction = "ease-in-out",
): string => {
    if (Array.isArray(property)) {
        return property.map((p) => transition(p, duration, easing)).join(", ");
    }
    return `${camelCaseToDash(property)} ${withUnit(duration, "ms")} ${easing}`;
};
