import { formatValue } from "./formatValue";

export const columnsToPercent = (columns: number, totalColumns = 12, gutter = 0) =>
    `calc(${gutter !== 0 ? `(100% + ${formatValue(gutter)})` : "100%"} / ${totalColumns} * ${columns})`;
