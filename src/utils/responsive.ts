import { isBrowser } from "@uxf/core/utils/isBrowser";
import facepaint from "facepaint";
import em from "polished/lib/helpers/em";
import hiDPI from "polished/lib/mixins/hiDPI";
import { css, CSSProperties, FlattenSimpleInterpolation } from "styled-components";
import { BreakpointsType } from "../types";
import { nonEmptyStyle } from "./nonEmptyStyle";

// media queries
export const mqBetween = (from: number, to: number): string =>
    `@media (min-width: ${em(from)}) and (max-width: ${em(to - 1)})`;
export const mqMin = (from: number): string => `@media (min-width: ${em(from)})`;
export const mqMax = (to: number): string => `@media (max-width: ${em(to - 1)})`;
export const mqHiDpi = (ratio = 2) => hiDPI(ratio);

// match media helpers
export const matchBetween = (from: number, to: number) =>
    isBrowser ? window.matchMedia(`(min-width: ${em(from)}) and (max-width: ${em(to - 1)})`).matches : false;
export const matchMin = (from: number) => (isBrowser ? window.matchMedia(`(min-width: ${em(from)})`).matches : false);
export const matchMax = (to: number) => (isBrowser ? window.matchMedia(`(max-width: ${em(to - 1)})`).matches : false);

// helpers for styled components media queries
export const mediaBetween = (from: number, to: number, style: FlattenSimpleInterpolation | null) => {
    return nonEmptyStyle(style)
        ? css`
              ${mqBetween(from, to)} {
                  ${style}
              }
          `
        : null;
};

export const mediaMin = (from: number, style: FlattenSimpleInterpolation | null) => {
    return nonEmptyStyle(style)
        ? css`
              ${mqMin(from)} {
                  ${style}
              }
          `
        : null;
};

export const mediaMax = (to: number, style: FlattenSimpleInterpolation | null) => {
    return nonEmptyStyle(style)
        ? css`
              ${mqMax(to)} {
                  ${style}
              }
          `
        : null;
};

export const mediaHiDpi = (style: FlattenSimpleInterpolation | null, ratio = 2) => {
    return nonEmptyStyle(style)
        ? css`
              ${mqHiDpi(ratio)} {
                  ${style}
              }
          `
        : null;
};

/* https://github.com/emotion-js/facepaint */

// helpers for facepaint responsive css props
export const facepaintMin = (breakpoints: BreakpointsType) => {
    const breakpointsArray = Object.values(breakpoints).slice(1) as number[];
    return facepaint(breakpointsArray.map((breakpoint) => mqMin(breakpoint)));
};

export const facepaintOnly = (breakpoints: BreakpointsType) => {
    const breakpointsArray = Object.values(breakpoints).slice(1) as number[];
    const breakpointsCount = breakpointsArray.length;
    return facepaint(
        breakpointsArray.map((breakpoint, index) => {
            if (index === 0) {
                return mqMax(breakpoint);
            }
            if (index === breakpointsCount - 1) {
                return mqMin(breakpoint);
            }
            return mqBetween(breakpointsArray[index - 1], breakpoint);
        }),
        { literal: true },
    );
};

export const resolveResponsiveProperties = (
    breakpoints: BreakpointsType,
    properties: Partial<Record<keyof CSSProperties, string | (string | null)[] | null>>,
) => {
    return Object.values(properties).find((p) => Array.isArray(p))
        ? css`
              ${facepaintMin(breakpoints)(properties)}
          `
        : ([properties] as FlattenSimpleInterpolation);
};
