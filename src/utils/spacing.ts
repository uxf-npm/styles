export const spacing = (input: number, factor = 8) => (input === 0 ? input : input * factor);
