export const getNumericValueFromArray = (
    matchBreakpoint: boolean,
    index: number,
    input?: number | (number | null)[],
) => {
    if (Array.isArray(input)) {
        if (matchBreakpoint && input[index]) {
            return input[index];
        }
        return input[0];
    }
    return input ?? null;
};
