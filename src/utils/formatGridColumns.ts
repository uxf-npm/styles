import { formatResponsiveValue } from "./formatValue";

export const formatGridColumns = (count?: number, size = "1fr") =>
    count && size ? `repeat(${count}, ${formatResponsiveValue(size)})` : null;

export const formatResponsiveGridColumns = (input?: number | (number | null)[]) => {
    if (Array.isArray(input)) {
        return input.map((item) => (item ? formatGridColumns(item) : null));
    }
    return formatGridColumns(input);
};
