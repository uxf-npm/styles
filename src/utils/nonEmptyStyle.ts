import { FlattenSimpleInterpolation } from "styled-components";

export function nonEmptyStyle(style?: FlattenSimpleInterpolation | null) {
    return Array.isArray(style) && !style.every((item) => !item);
}
