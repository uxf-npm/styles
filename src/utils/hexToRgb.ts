import parseToRgb from "polished/lib/color/parseToRgb";

export const hexToRgb = (color: string) => `rgb(${Object.values(parseToRgb(color)).join(",")})`;
