import rem from "polished/lib/helpers/rem";

// format CSS value (return string if number or string and null if other)
export const formatValue = <T = number | string>(input?: T, forceString = false) => {
    if (typeof input === "number" && !isNaN(input)) {
        if (input === 0 || (forceString && input)) {
            return input.toString();
        }
        return rem(input);
    } else if (typeof input === "string" && input !== "") {
        return input;
    }
    return null;
};

export const formatResponsiveValue = <T = number | string>(input?: T | (T | null)[], forceString = false) => {
    if (Array.isArray(input)) {
        return (input as T[]).map((item) => formatValue<T>(item, forceString));
    }
    return formatValue<T>(input as T, forceString);
};

export const formatResponsiveValueFromProperty = <T = Record<string, number | string>>(
    property: T,
    input?: keyof T | (keyof T | null)[],
    forceString = false,
) => {
    if (Array.isArray(input)) {
        return input.map((item) => (item ? formatValue(property[item], forceString) : undefined));
    }
    return input ? formatValue(property[input], forceString) : null;
};
