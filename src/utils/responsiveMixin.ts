import { css, FlattenSimpleInterpolation } from "styled-components";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";
import { nonEmptyStyle } from "./nonEmptyStyle";
import { mediaBetween, mediaMax, mediaMin, mqMax, mqMin } from "./responsive";

export const responsiveMixin = <B extends BreakpointsType>(
    breakpoints: B,
    style: FlattenSimpleInterpolation | null,
    condition?: ResponsiveConditionProperty<B>,
) => {
    if (typeof condition === "object") {
        const { from, to } = condition;

        if (from && to) {
            if (breakpoints[from] > breakpoints[to] && nonEmptyStyle(style)) {
                return css`
                    ${mqMax(breakpoints[to])} {
                        ${style};
                    }

                    ${mqMin(breakpoints[from])} {
                        ${style};
                    }
                `;
            }
            return mediaBetween(breakpoints[from], breakpoints[to], style);
        }

        if (from && !to) {
            return mediaMin(breakpoints[from], style);
        }

        if (to) {
            return mediaMax(breakpoints[to], style);
        }
    } else if (condition) {
        return style;
    }
    return null;
};
