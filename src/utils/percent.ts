import { trimTrailingZeros } from "@uxf/core/utils/trimTrailingZeros";

export const percent = (inputValue: number, maxValue = 100, precision = 2) => {
    if (inputValue === 0) {
        return inputValue;
    }
    return parseFloat(trimTrailingZeros(((inputValue / maxValue) * 100).toFixed(precision)));
};
