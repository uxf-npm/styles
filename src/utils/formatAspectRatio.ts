import { ResponsiveProperty } from "../types";
import { percent } from "./percent";

export const formatAspectRatio = (input?: number) => {
    if (input === 0) {
        return input.toString();
    }
    if (input) {
        return `${percent(1, input, 2)}%`;
    }
    return null;
};

export const formatResponsiveAspectRatio = (input?: ResponsiveProperty.AspectRatio) => {
    if (Array.isArray(input)) {
        return input.map((item) => (item ? formatAspectRatio(item) : null));
    }
    return formatAspectRatio(input);
};
