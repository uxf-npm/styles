import hideVisually from "polished/lib/mixins/hideVisually";
import { css } from "styled-components";
import { responsiveMixin } from "../utils/responsiveMixin";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";

export const responsiveHideVisually = <B extends BreakpointsType>(
    breakpoints: B,
    condition?: ResponsiveConditionProperty<B>,
) =>
    condition
        ? responsiveMixin(
              breakpoints,
              css`
                  &&& {
                      ${hideVisually()};
                  }
              `,
              condition,
          )
        : null;
