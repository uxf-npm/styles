import { css } from "styled-components";

export const noWrap = css`
    white-space: nowrap;
`;
