import { css } from "styled-components";

export const appearanceReset = css`
    appearance: none;
    background-color: transparent;
    border: 0;
    border-radius: 0;
    box-shadow: none;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
`;
