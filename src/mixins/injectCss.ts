import { css, FlattenSimpleInterpolation } from "styled-components";

export const injectCss = (style?: FlattenSimpleInterpolation) => {
    if (style && style.some((i) => i)) {
        return css`
            && {
                ${style}
            }
        `;
    }
    return null;
};
