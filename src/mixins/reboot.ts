import { css } from "styled-components";

const SYSTEM_FONTS =
    'system-ui, -apple-system, Segoe UI, Roboto, Ubuntu, Cantarell, Noto Sans, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"';
const SYSTEM_FONTS_MONOSPACE =
    "SFMono-Regular, Menlo, Monaco, Consolas, Roboto Mono, Ubuntu Monospace, Noto Mono, Oxygen Mono, Liberation Mono, monospace";

export interface RebootOptions {
    bodyBg?: string;
    bodyColor?: string;
    fontFamilyBase?: string;
    fontFamilyMonospace?: string;
    fontWeightBase?: number;
    lineHeightBase?: number;
    linkColor?: string;
}

export const reboot = (options?: RebootOptions) => css`
    /* stylelint-disable */
    *,
    *::before,
    *::after {
        background-repeat: no-repeat;
        box-sizing: border-box;
    }

    ::before,
    ::after {
        text-decoration: inherit;
        vertical-align: inherit;
    }

    html {
        cursor: default;
        font-family: sans-serif;
        font-size: 1rem;
        line-height: 1.15;
        overflow-wrap: break-word;
        tab-size: 4;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        -ms-overflow-style: scrollbar;
        -webkit-tap-highlight-color: rgba(#000, 0);
    }

    @-ms-viewport {
        width: device-width;
    }

    article,
    aside,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    main,
    nav,
    section {
        display: block;
    }

    body {
        background-color: ${options?.bodyBg ?? "#fff"};
        color: ${options?.bodyColor ?? "#000"};
        font-family: ${[options?.fontFamilyBase, SYSTEM_FONTS].filter((i) => i).join(", ")};
        font-size: 1em;
        font-weight: ${options?.fontWeightBase ?? 400};
        line-height: ${options?.lineHeightBase ?? 1};
        margin: 0;
        text-align: left;
    }

    [tabindex="-1"]:focus {
        outline: 0 !important;
    }

    hr {
        box-sizing: content-box;
        height: 0;
        overflow: visible;
    }

    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    p {
        margin: 0;
    }

    abbr[title],
    abbr[data-original-title] {
        border-bottom: 0;
        cursor: help;
        text-decoration: underline;
        text-decoration: underline dotted;
    }

    address {
        font-style: normal;
        line-height: inherit;
        margin: 0;
    }

    ol,
    ul,
    dl {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    ol ol,
    ul ul,
    ol ul,
    ul ol {
        margin: 0;
    }

    dt {
        font-weight: unset;
    }

    dd {
        margin: 0;
    }

    blockquote {
        margin: 0;
    }

    dfn {
        font-style: italic;
    }

    b,
    strong {
        font-weight: bolder;
    }

    small {
        font-size: 80%;
    }

    sub,
    sup {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline;
    }

    sub {
        bottom: -0.25em;
    }

    sup {
        top: -0.5em;
    }

    a {
        background-color: transparent;
        color: ${options?.linkColor ?? "#007bff"};
        text-decoration: underline;
        -webkit-text-decoration-skip: objects;
    }

    pre,
    code,
    kbd,
    samp {
        font-family: ${[options?.fontFamilyMonospace, SYSTEM_FONTS_MONOSPACE].filter((i) => i).join(", ")};
        font-size: 1em;
    }

    pre {
        margin: 0;
        overflow: auto;
        -ms-overflow-style: scrollbar;
    }

    figure {
        margin: 0;
    }

    img {
        border-style: none;
    }

    svg {
        overflow: hidden;
    }

    audio,
    canvas,
    iframe,
    img,
    svg,
    video {
        vertical-align: middle;
    }

    table {
        border-collapse: collapse;
    }

    caption {
        caption-side: bottom;
        color: unset;
        text-align: left;
        padding: 0;
    }

    th {
        text-align: inherit;
    }

    label {
        display: inline-block;
        margin: 0;
    }

    button {
        border-radius: 0;
    }

    button:focus {
        outline: 1px dotted;
        outline: 5px auto -webkit-focus-ring-color;
    }

    input,
    button,
    select,
    optgroup,
    textarea {
        font-family: inherit;
        font-size: inherit;
        line-height: inherit;
        margin: 0;
    }

    button,
    input {
        overflow: visible;
    }

    button,
    select {
        text-transform: none;
    }

    button,
    html [type="button"],
    [type="reset"],
    [type="submit"] {
        -webkit-appearance: button;
    }

    button::-moz-focus-inner,
    [type="button"]::-moz-focus-inner,
    [type="reset"]::-moz-focus-inner,
    [type="submit"]::-moz-focus-inner {
        border-style: none;
        padding: 0;
    }

    input[type="radio"],
    input[type="checkbox"] {
        box-sizing: border-box;
        padding: 0;
    }

    input[type="date"],
    input[type="time"],
    input[type="datetime-local"],
    input[type="month"] {
        -webkit-appearance: listbox;
    }

    [type="color"],
    [type="range"] {
        border-width: 0;
        padding: 0;
    }

    textarea {
        overflow: auto;
        resize: vertical;
    }

    fieldset {
        border: 0;
        margin: 0;
        min-width: 0;
        padding: 0;
    }

    legend {
        color: inherit;
        display: block;
        font-size: 1em;
        line-height: inherit;
        margin: 0;
        max-width: 100%;
        padding: 0;
        white-space: normal;
        width: 100%;
    }

    progress {
        vertical-align: baseline;
    }

    [type="number"]::-webkit-inner-spin-button,
    [type="number"]::-webkit-outer-spin-button {
        height: auto;
    }

    [type="search"] {
        outline-offset: -2px;
        -webkit-appearance: none;
    }

    [type="search"]::-webkit-search-cancel-button,
    [type="search"]::-webkit-search-decoration {
        -webkit-appearance: none;
    }

    ::-webkit-file-upload-button {
        font: inherit;
        -webkit-appearance: button;
    }

    output {
        display: inline-block;
    }

    summary {
        display: list-item;
        cursor: pointer;
    }

    template {
        display: none;
    }

    [hidden] {
        display: none;
    }

    svg:not([fill]) {
        fill: currentColor;
    }

    a,
    area,
    button,
    input,
    label,
    select,
    summary,
    textarea,
    [tabindex] {
        -ms-touch-action: manipulation;
        touch-action: manipulation;
    }

    [aria-busy="true"] {
        cursor: progress;
    }

    [aria-controls] {
        cursor: pointer;
    }

    [aria-disabled="true"],
    [disabled] {
        cursor: default;
    }

    [aria-hidden="false"][hidden] {
        display: initial !important;
    }

    [aria-hidden="false"][hidden]:not(:focus) {
        clip: rect(0, 0, 0, 0);
        position: absolute;
    }
    /* stylelint-enable */
`;
