import { css } from "styled-components";
import { appearanceReset } from "./appearanceReset";

export const inputReset = css`
    ${appearanceReset};

    /* stylelint-disable property-no-vendor-prefix, no-descending-specificity */
    &:invalid {
        box-shadow: none;
    }

    &::-moz-placeholder {
        opacity: 1;
    }

    &::-ms-clear,
    &::-ms-reveal {
        display: none;
    }

    &::-webkit-calendar-picker-indicator,
    &::-webkit-clear-button,
    &::-webkit-inner-spin-button,
    &::-webkit-outer-spin-button {
        margin: 0;
        -webkit-appearance: none;
    }

    &[type="date"],
    &[type="datetime"],
    &[type="datetime-local"],
    &[type="month"],
    &[type="number"],
    &[type="time"],
    &[type="week"] {
        -webkit-appearance: none;
        -moz-appearance: textfield;
    }
    /* stylelint-enable */
`;
