import { css } from "styled-components";
import { ColorProperty } from "../types";

export const inputAutofill = (background: ColorProperty, color: ColorProperty) => css`
    /* stylelint-disable property-no-vendor-prefix, no-descending-specificity */
    &:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 1000px ${background} inset;
        -webkit-text-fill-color: ${color} !important;
    }
    &:-moz-autofill {
        -moz-box-shadow: 0 0 0 1000px ${background} inset;
        -moz-text-fill-color: ${color} !important;
    }
    &:-o-autofill {
        -o-box-shadow: 0 0 0 1000px ${background} inset;
        -o-text-fill-color: ${color} !important;
    }
    &:-khtml-autofill {
        -khtml-box-shadow: 0 0 0 1000px ${background} inset;
        -khtml-text-fill-color: ${color} !important;
    }
    /* stylelint-enable */
`;
