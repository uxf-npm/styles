import { responsiveMixin } from "../utils/responsiveMixin";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";
import { noWrap } from "./noWrap";

export const responsiveNoWrap = <B extends BreakpointsType>(
    breakpoints: B,
    condition?: ResponsiveConditionProperty<B>,
) => (condition ? responsiveMixin(breakpoints, noWrap, condition) : null);
