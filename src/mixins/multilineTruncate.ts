import { css } from "styled-components";

export const multilineTruncate = (lines?: number) =>
    lines
        ? css`
              /* stylelint-disable property-no-vendor-prefix, value-no-vendor-prefix */
              display: -webkit-box;
              -webkit-line-clamp: ${lines};
              -webkit-box-orient: vertical;
              overflow: hidden;
              /* stylelint-enable */
          `
        : null;
