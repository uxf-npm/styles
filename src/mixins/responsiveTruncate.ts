import { css } from "styled-components";
import { responsiveMixin } from "../utils/responsiveMixin";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";
import { truncate } from "./truncate";

export const responsiveTruncate = <B extends BreakpointsType>(
    breakpoints: B,
    condition?: ResponsiveConditionProperty<B>,
) =>
    condition
        ? responsiveMixin(
              breakpoints,
              css`
                  ${truncate}
              `,
              condition,
          )
        : null;
