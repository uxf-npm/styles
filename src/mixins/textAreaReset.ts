import { css } from "styled-components";
import { appearanceReset } from "./appearanceReset";

export const textAreaReset = css`
    ${appearanceReset};

    /* stylelint-disable property-no-vendor-prefix, no-descending-specificity */
    &:invalid {
        box-shadow: none;
    }

    &::-moz-placeholder {
        opacity: 1;
    }
    /* stylelint-enable */
`;
