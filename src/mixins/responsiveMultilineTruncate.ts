import { responsiveMixin } from "../utils/responsiveMixin";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";
import { multilineTruncate } from "./multilineTruncate";

export const responsiveMultilineTruncate = <B extends BreakpointsType>(
    breakpoints: B,
    lines?: number,
    condition?: ResponsiveConditionProperty<B>,
) => (lines ? responsiveMixin(breakpoints, multilineTruncate(lines), condition) : null);
