import { css } from "styled-components";
import { formatResponsiveAspectRatio } from "../utils/formatAspectRatio";
import { facepaintMin } from "../utils/responsive";
import { BreakpointsType, ResponsiveProperty } from "../types";

export const aspectRatio = <B extends BreakpointsType>(breakpoints: B, ratio: ResponsiveProperty.AspectRatio) =>
    (Array.isArray(ratio) && ratio.some((i) => i)) || ratio
        ? css`
              &::before {
                  content: "";
                  display: block;
                  width: 100%;

                  ${facepaintMin(breakpoints)({
                      paddingBottom: formatResponsiveAspectRatio(ratio),
                  })};
              }
          `
        : null;
