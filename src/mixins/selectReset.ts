import { Property } from "csstype";
import { css } from "styled-components";
import { appearanceReset } from "./appearanceReset";

export const selectReset = (fontFamily?: Property.FontFamily) => css`
    ${appearanceReset};

    /* stylelint-disable property-no-vendor-prefix, no-descending-specificity */
    &:invalid {
        box-shadow: none;
    }

    &::-ms-expand {
        display: none;
    }
    /* stylelint-enable */

    option {
        font-family: ${fontFamily ?? null};
        font-size: 0.8rem;
        font-weight: 400;
    }
`;
