import { css } from "styled-components";
import { responsiveMixin } from "../utils/responsiveMixin";
import { BreakpointsType, ResponsiveConditionProperty } from "../types";

export const responsiveHidden = <B extends BreakpointsType>(
    breakpoints: B,
    condition?: ResponsiveConditionProperty<B>,
) =>
    condition
        ? responsiveMixin(
              breakpoints,
              css`
                  display: none !important;
              `,
              condition,
          )
        : null;
